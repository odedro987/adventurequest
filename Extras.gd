extends Node

var colors = {
	"GOLD" : Color(1.0, 0.843, 0.0, 1.0),
	"BLACK" : Color(0.0, 0.0, 0.0, 1.0),
	"WHITE" : Color(1.0, 1.0, 1.0, 1.0),
	"STRONG_CYAN" : Color(0.0, 0.745, 0.745, 1.0)
}

#Returns a random number in range.
func randRange(from, to):
	return floor(rand_range(from, to + 0.999))

#Colors parts of a text from a color array.
func colorText(text, colors):
	var newText = text
	var openColorTag = "[color=#"
	var closeColorTag = "]"
	var endColorTag = "[/color]"
	var currentIndex = newText.find("|", 0)
	
	while currentIndex != -1:
		newText = replaceFirstOccurence(newText,\
							  "|" + newText[currentIndex + 1] + "|",\
							  openColorTag + colors[int(newText[currentIndex + 1]) - 1].to_html(false) + closeColorTag)
		currentIndex = newText.find("|", currentIndex)
		newText = replaceFirstOccurence(newText, "|", endColorTag)
		currentIndex = newText.find("|", currentIndex)
	return newText

#Replaces the first occurence of 'fron' to 'to'.
func replaceFirstOccurence(text, from, to):
	var newText = text
	var currentIndex = 0
	currentIndex = text.find(from, currentIndex)
	
	for i in range(from.length()):
		newText[currentIndex] = ""
	
	return newText.insert(currentIndex, to)
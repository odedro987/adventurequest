extends Node

#==========================================================GAME VARIABLES============================================================
var SCREEN_SIZE = Vector2(Globals.get("display/width"), Globals.get("display/height"))
var gameMultiplier = 1
var minExp = 20
var minGold = 30
var ATTRIBUTES = {
	FIRE = "Fire",
	WATER = "Water",
	AIR = "Air",
	EARTH = "Earth",
	GRASS = "Grass",
	ICE = "Ice",
	LIGHTNING = "Lightning",
	HOLY = "Holy",
	DARK = "Dark"
}
#Quests related variables.
var questBattleCount = 2
var currentQuestBattle = 1
var questBattleScenes = ["Random"]
#================================================================END=================================================================
#===========================================================FUNCTIONALITY============================================================
#Returns an array of strong-weak attributes of an attribute.
func getAtrributesEffectiveness(attribute):
	if(attribute == ATTRIBUTES.FIRE):        return [[ATTRIBUTES.GRASS, ATTRIBUTES.ICE]     , [ATTRIBUTES.WATER, ATTRIBUTES.EARTH]]
	elif(attribute == ATTRIBUTES.WATER):     return [[ATTRIBUTES.FIRE, ATTRIBUTES.EARTH]    , [ATTRIBUTES.GRASS, ATTRIBUTES.LIGHTNING]]
	elif(attribute == ATTRIBUTES.AIR):       return [[ATTRIBUTES.EARTH, ATTRIBUTES.GRASS]   , [ATTRIBUTES.LIGHTNING, ATTRIBUTES.ICE]]
	elif(attribute == ATTRIBUTES.EARTH):     return [[ATTRIBUTES.LIGHTNING, ATTRIBUTES.FIRE], [ATTRIBUTES.AIR, ATTRIBUTES.WATER]]
	elif(attribute == ATTRIBUTES.GRASS):     return [[ATTRIBUTES.ICE, ATTRIBUTES.WATER]     , [ATTRIBUTES.AIR, ATTRIBUTES.FIRE]]
	elif(attribute == ATTRIBUTES.ICE):       return [[ATTRIBUTES.AIR, ATTRIBUTES.LIGHTNING] , [ATTRIBUTES.FIRE, ATTRIBUTES.GRASS]]
	elif(attribute == ATTRIBUTES.LIGHTNING): return [[ATTRIBUTES.WATER, ATTRIBUTES.AIR]     , [ATTRIBUTES.EARTH, ATTRIBUTES.ICE]]
	elif(attribute == ATTRIBUTES.HOLY):      return [[ATTRIBUTES.DARK], [ATTRIBUTES.HOLY]]
	elif(attribute == ATTRIBUTES.DARK):      return [[ATTRIBUTES.HOLY], [ATTRIBUTES.DARK]]
	else: return []
#================================================================END=================================================================
#==========================================================PLAYER VARIABLES==========================================================
var currentGold = 0
var playerProperties = {
	"Name" : "Oded",
	"MaxHealth" : getMaxHealthByLevel(1),
	"MaxMana" : getMaxManaByLevel(1),
	"Experience" : 0,
	"MaxExperience" : getNextLevelMaxExperience(1),
	"Level" : 1,
	"Damage" : 5,
	"Spells" : [ "Laser Beam", "Lightning Strike", "Ice Shard" ],
	"Weapons" : [ "Broad Sword", "Katana", "Wooden Staff", "Battle Hammer", "Battle Axe"],
}

var currentWeapon = getPlayerProperty("Weapons")[0]

func getPlayerProperty(property):
	return playerProperties[property]

func setPlayerProperty(property, newValue):
	playerProperties[property] = newValue

func getCurrentGold():
	return currentGold
#================================================================END=================================================================
#===========================================================GAME FORMULAE============================================================
func getNextLevelMaxExperience(currentLevel):
	if (currentLevel == 1): return 100
	else: return round(getNextLevelMaxExperience(currentLevel - 1) * (1.1))

func getGoldRewardByMonsterLevel(monsterLevel):
	return round(minGold + 2 * abs(sqrt(monsterLevel) * (monsterLevel / 4))) * gameMultiplier

func getXpRewardByMonsterLevel(monsterLevel):
	return getGoldRewardByMonsterLevel(monsterLevel)

func getMaxHealthByLevel(currentLevel):
	return round((23.8 * ((5.25 + 0.5625 * currentLevel + 0.00375 * (currentLevel * currentLevel)) + (1 + 0.066 * currentLevel)) * 1 / 1.4))

func getMaxManaByLevel(currentLevel):
	return round((4.1 * (32 + (6.1 + 2.3375 * currentLevel + 0.01125 * (currentLevel * currentLevel)) * min(1, 1 / min(currentLevel * 2.1462 + 5.7076, 200)))))
#================================================================END=================================================================
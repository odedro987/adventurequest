extends Node2D

func _ready():
	global.questBattleCount = 3
	global.currentQuestBattle = 1
	global.questBattleScenes = ["Grass", "Random", "Grass"]
	get_node("toBattleBtn").connect("pressed", self, "moveToBattle")

func moveToBattle():
	get_tree().change_scene("res://States/ToBattleScene.tscn")
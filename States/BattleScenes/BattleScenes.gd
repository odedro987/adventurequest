extends Node

#Variables.
var baseBiomeWidth  = 240
var baseBiomeHeight = 32
#Biome types.
var biomeTypes      = [ "Grass", "Dungeon" ]

#Returns a random arch type biome.
func getRandomBiomeType():
	randomize()
	return biomeTypes[int(rand_range(0, biomeTypes.size()))]

func _ready():
	pass
tool
extends Node2D

#Editor tools.
export(String) var enemy = "BlueSlme" setget spawnEnemy
export(String) var battleScene = "Grass" setget loadBattleScene

#Preloads.
var enemyBank = preload("res://Entities/Enemies.tscn").instance()
var weaponBank = preload("res://Weapons/WeaponBank.tscn").instance()
var spellBank = preload("res://Spells/SpellBank.tscn").instance()
var battleSceneBank = preload("res://States/BattleScenes/BattleScenes.tscn").instance()

#Variables.
var isPlayerTurn
var isPlayerAttacking
var isBattleEnd
var enemyAttackType
var attackType

#Node variables.
var enemyNode
var enemyTypeNode
var playerNode
var playerWeapon
var spellNode
var weaponMenu
var spellMenu

func _ready():
	#Init everything in the scene.
	initBattleScene()
	#Connecting actions to GUI.
	connectActions()
	#Initializing input.
	set_process_input(true)

func initBattleScene():
	if(global.currentQuestBattle <= global.questBattleCount):
		#Initializing variables.
		enemyAttackType   = "DefaultAttack"
		attackType        = "DefaultAttack"
		isPlayerTurn      = true
		isPlayerAttacking = false
		isBattleEnd       = false
		enemyNode         = get_node("Enemy")
		enemyTypeNode     = get_node("Enemy").get_child(0)
		playerNode        = get_node("Player")
		playerWeapon      = get_node("Player/Weapon")
		spellNode         = get_node("Spell")
		weaponMenu        = get_node("BattleMenu/EquipmentMenu/WeaponsMenu")
		spellMenu         = get_node("BattleMenu/BasicMenu/SpellsMenu")
		if(global.currentQuestBattle == 1):
			initGlobalPlayer()
		#Loading the battle scene.
		randomize()
		loadBattleScene(global.questBattleScenes[0] if global.questBattleScenes.size() == 1 else global.questBattleScenes[global.currentQuestBattle - 1])
		#Adding a random monster by the battle scene biome and rarity.
		spawnEnemy(enemyBank.getRandomMonsterByRarityAndOrBiome(Extras.randRange(1, 4),\
				   get_node("BattleScene").archBiomeType))
		#Resetting the floor position.
		get_node("BattleScene").set_pos(Vector2(448, 660))
		#Resetting the position of the player and enemy on top of the battle scene floor.
		playerNode.set_pos(Vector2(100, get_node("BattleScene").get_pos().y - battleSceneBank.baseBiomeHeight*4))
		enemyNode.set_pos(Vector2(960 - 200, get_node("BattleScene").get_pos().y - battleSceneBank.baseBiomeHeight*4))
		#Adding the weapon to the player, resetting its position and initializing its properties.
		if(playerWeapon.get_children().size() > 0):
			playerWeapon.remove_child(playerWeapon.get_child(0))
		playerWeapon.add_child(weaponBank.get_node(global.currentWeapon.replace(" ", "")).duplicate())
		playerWeapon.get_child(0).get_node("Sprite").set_pos(playerNode.weaponHoldingPos)
		initWeapon()
		#Setting the droplist menus from the global player variables.
		get_node("BattleMenu").addSpells(playerNode.spells)
		get_node("BattleMenu").addWeapons(playerNode.weapons)
		#Disabling the current weapon item.
		weaponMenu.get_popup().set_item_disabled(weaponMenu.getItemIndexByName(global.currentWeapon), true)
		#Setting the HUDs.
		if(global.currentQuestBattle == 1):
			updateHUD("PlayerHUD", playerNode)
		updateHUD("MonsterHUD", enemyNode, enemyTypeNode)
		#Updating the battle menu.
		updateBattleMenu()
		#Incrementing global battle index.
		global.currentQuestBattle = global.currentQuestBattle + 1
#=====================================================GLOBALS HANDLING===================================================================
#Loads player stats from the global player variable.
func initGlobalPlayer():
	get_node("Player").setPlayer(global.getPlayerProperty("Name"),\
								 global.getPlayerProperty("MaxHealth"),\
								 global.getPlayerProperty("MaxMana"),\
								 global.getPlayerProperty("Experience"),\
								 global.getPlayerProperty("MaxExperience"),\
								 global.getPlayerProperty("Level"),\
								 global.getPlayerProperty("Damage"),\
								 global.getPlayerProperty("Spells"),\
								 global.getPlayerProperty("Weapons"))

#Initializes the weapon from the global weapon variable.
func initWeapon():
	var weaponName = playerWeapon.get_child(0).get_name()
	playerWeapon.damage = weaponBank.getProperty(weaponName, "BaseDamage")
	playerWeapon.attribute = weaponBank.getProperty(weaponName, "Attribute")

#Initializes the weapon from the global weapon variable.
func initSpell():
	var spellName = spellNode.get_child(0).get_name()
	spellNode.damage = spellBank.getProperty(spellName, "BaseDamage")
	spellNode.attribute = spellBank.getProperty(spellName, "Attribute")
	spellNode.manaCost = spellBank.getProperty(spellName, "ManaCost")

#Checks if suppose to level up.
func checkLevelUp():
	var isLevelUp = global.getPlayerProperty("Experience") >= global.getPlayerProperty("MaxExperience")
	if(isLevelUp):
		doPopup("LevelUpPopup", "Congratulations!", "Congratulations! You've leveled up!\nYou'll get a bit stronger and can use better equipment.")
		global.setPlayerProperty("Experience", 0)
		global.setPlayerProperty("Level", int(global.getPlayerProperty("Level")) + 1)
		global.setPlayerProperty("MaxExperience", global.getNextLevelMaxExperience(global.getPlayerProperty("Level")))
		global.setPlayerProperty("MaxHealth", global.getMaxHealthByLevel(global.getPlayerProperty("Level")))
		global.setPlayerProperty("MaxMana", global.getMaxManaByLevel(global.getPlayerProperty("Level")))
#============================================================END=========================================================================
#=======================================================HUD HANDLING=====================================================================
#Initializes HUD by its type.
func initHUD(hudType, name, avatar, hpBar, mpBar):
	var hud = get_node(hudType)
	hud.setName(name)
	hud.setAvatar(avatar)
	hud.setBar(hud.hpContainer.get_node("HpLabel"), "HP", hpBar)
	hud.setBar(hud.mpContainer.get_node("MpLabel"), "MP", mpBar)
	if(hudType == "PlayerHUD"): hud.setBar(hud.expContainer.get_node("ExpLabel"),\
										  "EXP", Vector2(playerNode.experience,\
										  playerNode.maxExperience))
#Updates HUD by entityType.
func updateHUD(hudNode, entityNode, entityTypeNode=entityNode):
	initHUD(hudNode,\
			"Lv." + str(entityNode.level) + " " + str(entityNode.name),\
			entityTypeNode.get_node("Avatar").get_texture(),\
			Vector2(entityNode.maxHealth,\
			entityNode.maxHealth),\
			Vector2(entityNode.maxMana,\
			entityNode.maxMana))
	
#Updates BattleMenu's state depends on the isPlayerTurn bool.
func updateBattleMenu():
	if(!self.isPlayerAttacking): get_node("BattleMenu").show()
	else: get_node("BattleMenu").hide()
#============================================================END============================================================================
#======================================================COMBAT HANDLING======================================================================
#Updates the battle menu based on whether the player is attacking and changes turn.
func nextTurn():
	updateBattleMenu()
	if(!isBattleEnd && !isPlayerTurn): attackPlayer("DefaultAttack")

#Calculates the damage amplifier with consideration to attributes.
func calculateAtrributeDamage(attacker, receiver):
	var effectiveness = global.getAtrributesEffectiveness(attacker)
	for i in range(effectiveness.size()):
		for j in range(effectiveness[i].size()):
			if(effectiveness[i][j] == receiver):
				if(i == 0): return 1.5
				else:       return 0.5
	return 1.0

#Incantates spell
func incantateSpell(ID):
	#Updating the selected item.
	spellMenu.updateSelectedItem(ID)
	#Setting isPlayerAttacking to true.
	self.isPlayerAttacking = true
	#Setting attackType to spell attack.
	self.attackType = "SpellAttack"
	changeSpellById(ID)
	#Lowering MpBar and player mp.
	playerNode.mana = playerNode.mana - spellNode.manaCost
	get_node("PlayerHUD").setBar(get_node("PlayerHUD").mpContainer.get_node("MpLabel"),\
									 "MP",\
									 Vector2(playerNode.mana,\
									 playerNode.maxMana))
	playerNode.get_node("AnimationPlayer").play("CastSpell")
	playerWeapon.get_child(0).get_node("AnimationPlayer").play("CastSpell")
	
#Casts spell
func castSpell():
	spellNode.get_child(0).get_node("AnimationPlayer").play("Cast")

#Attacks the enemy, plays the player "Attack" animation by attackType.
func attackEnemy(attackType):
	#Setting isPlayerAttacking to true.
	self.isPlayerAttacking = true
	#Setting attackType to default attack.
	self.attackType = "DefaultAttack"
	playerNode.get_node("AnimationPlayer").play(attackType)
	playerWeapon.get_child(0).get_node("AnimationPlayer").play(attackType)

#Hurts enemy, plays the "Hurt" animation.
func hurtEnemy():
	endBattlePopup()
	if(!isBattleEnd):
		#Setting isPlayerTurn to false.
		self.isPlayerTurn = false
		enemyTypeNode.get_node("AnimationPlayer").play("Hurt")

#Damages enemy, decreases its hp and showing the fading damage node.
func damageEnemy():
	if(!isBattleEnd):
		var playerBaseDamage = 0
		var damageAttribute = global.ATTRIBUTES.FIRE
		#Setting base damage and attribute depends on attackType.
		if(self.attackType == "DefaultAttack"): 
			playerBaseDamage = playerWeapon.damage
			damageAttribute  = playerWeapon.attribute
		elif(self.attackType == "SpellAttack"): 
			playerBaseDamage = spellNode.damage
			damageAttribute  = spellNode.attribute
		#Randomizing the attack range and taking attributes into consideration.
		randomize()
		var inflictedDamage = round(round(rand_range(0.80 * playerBaseDamage, 1.20 * playerBaseDamage))\
									* calculateAtrributeDamage(damageAttribute, enemyNode.attribute))
		var monsterHUD = get_node("MonsterHUD")
		#Lowering HpBar.
		monsterHUD.setBar(monsterHUD.hpContainer.get_node("HpLabel"),\
						  "HP",\
						  Vector2(monsterHUD.hpVector.x - inflictedDamage,\
						  monsterHUD.hpVector.y))
		#Calling show fading damage.
		showFadingDamageAbove("Enemy", inflictedDamage, damageAttribute)
		endBattlePopup()

#Attacks the player, plays the enemy "Attack" animation by attackType.
func attackPlayer(attackType):
	#Setting isPlayerTurn to true.
	self.isPlayerTurn = true
	#Setting isPlayerAttacking to false.
	self.isPlayerAttacking = false
	enemyAttackType = attackType
	enemyTypeNode.get_node("AnimationPlayer").play(attackType)

#Hurts player, plays the "Hurt" animation.
func hurtPlayer():
	endBattlePopup()
	if(!isBattleEnd):
		playerNode.get_node("AnimationPlayer").play("Hurt")
		playerWeapon.get_child(0).get_node("AnimationPlayer").play("Hurt")

#Damages player, decreases its hp and showing the fading damage node.
func damagePlayer():
	if(!isBattleEnd):
		var enemyDamage = 0
		if(enemyAttackType == "DefaultAttack")   : enemyDamage = enemyBank.getProperty(enemyTypeNode.get_name(), "BaseDamage")
		elif(enemyAttackType == "SpecialAttack1"): pass #TODO: elif for calculating attribute of magic on enemy.
		elif(enemyAttackType == "SpecialAttack2"): pass #TODO: elif for calculating attribute of magic on enemy.
		playerNode.health = playerNode.health - enemyDamage
		#Lowering HpBar.
		get_node("PlayerHUD").setBar(get_node("PlayerHUD").hpContainer.get_node("HpLabel"),\
									 "HP",\
									 Vector2(playerNode.health,\
									 playerNode.maxHealth))
		showFadingDamageAbove("Player", enemyDamage, enemyNode.attribute)
		endBattlePopup()

#Flees to town.
func fleeToTown():
	get_tree().change_scene("res://States/MainState.tscn")
#============================================================END===========================================================================
#======================================================POPUP HANDLING======================================================================
#Initializes the popup and calling it.
func doPopup(popupType, title, text, callbackFunction=null):
	var node = get_node(popupType)
	node.set_title(title)
	node.set_text(text)
	node.set_pos(Vector2(global.SCREEN_SIZE.x / 2 - node.get_size().x / 2, global.SCREEN_SIZE.y / 2 - node.get_size().y / 2))
	if(callbackFunction != null): node.connect("confirmed", self, callbackFunction)
	node.popup()

#Calls end battle popup. If both entities didn't die, do nothing.
func endBattlePopup():
	if(!isBattleEnd && playerNode.health <= 0):
		#Ending the battle.
		isBattleEnd = true
		#TODO: Some code for gold lost after battle ???
		doPopup("EndBattlePopup",\
				"Lost The Battle...",\
				Extras.colorText("You've lost the battle.\nYou lost |1|some gold|.\nNow return to the town.", [Extras.colors["GOLD"]]),\
				"fleeToTown")
	elif(!isBattleEnd && get_node("MonsterHUD").hpVector.x <= 0):
		#Checking if should procceed or go back.
		var nextAction = "initBattleScene" if global.currentQuestBattle <= global.questBattleCount else "fleeToTown"
		#Ending the battle.
		isBattleEnd = true
		var goldReward = global.getGoldRewardByMonsterLevel(enemyNode.level)
		var xpReward = global.getXpRewardByMonsterLevel(enemyNode.level)
		#Updating the global gold variable.
		global.currentGold += goldReward
		#Updating the global player experience variable.
		global.setPlayerProperty("Experience", int(global.getPlayerProperty("Experience")) + xpReward)
		#Calling improved popup with colored text for rewards.
		var goldRewardText = "|1|" + str(goldReward) + "| pieces of |1|gold|";
		var expRewardText  = "|2|" + str(xpReward) + " EXP|";
		doPopup("EndBattlePopup",\
				"Won The Battle!",\
				Extras.colorText("You've won the battle.\nYou won " + goldRewardText + " and " + expRewardText + ".\nNow return to the town.",\
				[Extras.colors["GOLD"], Extras.colors["STRONG_CYAN"]]),\
				nextAction)
		checkLevelUp()

#Calls flee popup.
func fleePopup():
	#Randomizing a number between 0 and the enemy max flee block.
	randomize()
	var successful = randi() % 10
	#If the random number is higher than the enemy flee block, flee is successful.
	if(successful > enemyBank.getProperty(enemyTypeNode.get_name(), "BaseFleeBlock")):
		doPopup("FleePopup", "Fled Successfully", "You have fled successfully, and will go back to town.", "fleeToTown")
	else:
		doPopup("FleePopup", "Failed", "You can't flee.")
#============================================================END===========================================================================
#========================================================NODE HANDLING=====================================================================
#Shows fading damage above the player/enemy.
func showFadingDamageAbove(entity, damage, attribute):
	var spriteSize
	var spriteSheetHeight
	var spriteSheetWidth
	var pos
	#Initializing the above variables depends on the entity.
	if(entity == "Player"):
		spriteSize = playerNode.get_node("Sprite").get_texture().get_size()
		spriteSheetHeight = playerNode.get_node("Sprite").get_hframes()
		spriteSheetWidth = playerNode.get_node("Sprite").get_vframes()
		pos = playerNode.get_pos()
	else:
		spriteSize = enemyTypeNode.get_node("Sprite").get_texture().get_size()
		spriteSheetHeight = enemyTypeNode.get_node("Sprite").get_vframes()
		spriteSheetWidth = enemyTypeNode.get_node("Sprite").get_hframes()
		pos = enemyNode.get_pos()
	#Changing the position of the fading damage node based on the above variables.
	get_node("FadingDamageContaier").changePos(Vector2((pos.x + ((spriteSize.width / spriteSheetWidth)) / 2) - 38,\
											   pos.y -(((spriteSize.height / spriteSheetHeight) * 4)) - 32 - 80))
	#Calling the fading damage node.
	get_node("FadingDamageContaier").showDamage(String(damage), attribute)

#Changes weapon. 
func changeWeaponById(ID):
	if(playerWeapon != null):
		#Removing all existing children nodes if any.
		if(playerWeapon.get_children().size() > 0): playerWeapon.remove_child(playerWeapon.get_child(0))
	#Updating the selected item.
	weaponMenu.updateSelectedItem(ID)
	#Clearing previous disabled weapon and disabling the current weapon menu item.
	weaponMenu.clearItemDisability()
	weaponMenu.get_popup().set_item_disabled(ID, true)
	#Adding the weapon node based on the popup menu item clicked.
	playerWeapon.add_child(weaponBank.get_node(weaponBank.getWeaponIdByName(weaponMenu.getSelectedItemName())).duplicate())
	#Recalling the player's stance animation to avoid weird weapon position placement.
	playerNode.get_node("AnimationPlayer").play("Stance")
	playerWeapon.get_child(0).get_node("Sprite").set_pos(playerNode.weaponHoldingPos)
	#Updating the global weapon variable.
	global.currentWeapon = weaponMenu.getSelectedItemName()
	initWeapon()
	#Returning to the basic battle menu.
	get_node("BattleMenu").returnToBasicMenu()

#Changes spell. 
func changeSpellById(ID):
	if(spellNode != null):
		#Removing all existing children nodes if any.
		if(spellNode.get_children().size() > 0): spellNode.remove_child(spellNode.get_child(0))
	#Adding the spell node based on the popup menu item clicked.
	spellNode.add_child(spellBank.get_node(spellBank.getSpellIdByName(spellMenu.getSelectedItemName())).duplicate())
	spellNode.get_child(0).get_node("Sprite").set_pos(Vector2(160 * 4 / 2 - 16, -8))
	initSpell()

#Disables spells if mp is too low.
func disableSpells():
	for spell in range(spellMenu.items.size()):
		if(playerNode.mana - spellBank.getProperty(spellBank.getSpellIdByName(spellMenu.items[spell][0]), "ManaCost") < 0):
			spellMenu.get_popup().set_item_disabled(spell, true)

#Sets a new position for a node.
func changePos(nodeToChange, x, y):
	var currentPos = get_node(nodeToChange).get_node("Sprite").get_pos()
	get_node(nodeToChange).get_node("Sprite").set_pos(Vector2(currentPos + x, currentPos + y))
	
#Sets a new position for a weapon.
func changeWeaponPos(newPos):
	playerWeapon.get_child(0).get_node("Sprite").set_pos(Vector2(playerNode.weaponHoldingPos.x + newPos.x, playerNode.weaponHoldingPos.y + newPos.y))
#============================================================END===========================================================================
#=======================================================INPUT HANDLING=====================================================================
#Input handling.
func _input(event):
	#Adding a random monster by the battle scene biome and rarity WHEN spawn_enemy action is called.
	if(event.is_action_pressed("spawn_enemy")): 
		randomize()
		spawnEnemy(enemyBank.getRandomMonsterByRarityAndOrBiome(Extras.randRange(enemyBank.minRarity,\
				   enemyBank.maxRarity), get_node("BattleScene").archBiomeType))
	#Changing to a random biome WHEN spawn_enemy action is called.
	#And adding a random monster by the battle scene biome and rarity.
	if(event.is_action_pressed("change_biome")):
		loadBattleScene("Random")
		randomize()
		spawnEnemy(enemyBank.getRandomMonsterByRarityAndOrBiome(Extras.randRange(enemyBank.minRarity,\
				   enemyBank.maxRarity), get_node("BattleScene").archBiomeType))

#Connects actions to GUI.
func connectActions():
	#Attack enemy connection.
	get_node("BattleMenu/BasicMenu/AttackButton").connect("pressed", self, "attackEnemy", ["SwingAttack"])
	#Flee popup connection.
	get_node("BattleMenu/BasicMenu/FleeButton").connect("pressed", self, "fleePopup")
	#WeaponMenu popup connection.
	weaponMenu.get_popup().connect("item_pressed", self, "changeWeaponById")
	#SpellsMenu popup connection.
	spellMenu.get_popup().connect("item_pressed", self, "incantateSpell")
	spellMenu.connect("pressed", self, "disableSpells")
#============================================================END===========================================================================
#=======================================================SCENE LOADING======================================================================
#Load battle scene.
func loadBattleScene(archBiomeType):
	#Setting the editor variable.
	battleScene = archBiomeType
	#Temp variable.
	var biomeType = archBiomeType
	#If clause for randomly chosen biome.
	if(archBiomeType == "Random"):
		biomeType = battleSceneBank.getRandomBiomeType()
	#As long as the node isn't null.
	if(get_node("BattleScene") != null):
		#Removing all existing children nodes if any.
		if(get_node("BattleScene").get_children().size() > 0): get_node("BattleScene").remove_child(get_node("BattleScene").get_child(0))
		#Adding the sprite.
		get_node("BattleScene").add_child(battleSceneBank.get_node(biomeType).duplicate())
		#Resetting the position on top of the node.
		get_node("BattleScene").get_node(biomeType).get_node("Sprite").set_pos(Vector2(-4, -96))
		#Setting variables.
		get_node("BattleScene").archBiomeType = biomeType

#Spawns enemy.
func spawnEnemy(enemyToSpawn):
	#Setting the editor variable.
	enemy = enemyToSpawn
	#As long as the node isn't null.
	if(enemyNode != null):		#Removing all existing children nodes if any.
		if(enemyNode.get_children().size() > 0): enemyNode.remove_child(enemyTypeNode)
		#Adding the sprite.
		enemyNode.add_child(enemyBank.get_node(enemyToSpawn).duplicate())
		#Initializing EnemyTypeNode variable.
		enemyTypeNode = get_node("Enemy").get_child(0)
		#Resetting the position relative to the sprite height. // 0 - height * scale / 2 - 32(node size).
		var spriteSize = enemyNode.get_node(enemyToSpawn).get_node("Sprite").get_texture().get_size()
		var spriteSheetHeight = enemyNode.get_node(enemyToSpawn).get_node("Sprite").get_vframes()
		enemyNode.get_node(enemyToSpawn).get_node("Sprite").set_pos(Vector2(0, -(((spriteSize.height / spriteSheetHeight) * 4) / 2) - 32))
		#Setting variables.
		enemyNode.maxHealth = enemyBank.getProperty(enemyToSpawn, "BaseHealth")
		enemyNode.damage    = enemyBank.getProperty(enemyToSpawn, "BaseDamage")
		enemyNode.rarity    = enemyBank.getProperty(enemyToSpawn, "Rarity")
		enemyNode.biome     = enemyBank.getProperty(enemyToSpawn, "Biome")
		enemyNode.attribute = enemyBank.getProperty(enemyToSpawn, "Attribute")
		enemyNode.name      = enemyBank.getProperty(enemyToSpawn, "Name")
		randomize()
		enemyNode.level = (int(rand_range((playerNode.level / 1.5), (playerNode.level * 1.5))) + 1) * (1 + int(enemyNode.rarity) / 10)
		#Updating monsterHUD
		updateHUD("MonsterHUD", enemyNode, enemyTypeNode)
#============================================================END==========================================================================
extends Node

#Variables.
var spellProperties = SpellProperties.spellProperties
var spellTypes = spellProperties.keys()

#Returns the certain property of a certian skillType from the hashmap.
func getProperty(spellTypes, property):
	return spellProperties[spellTypes][property]

#Returns the spell name without spaces.
func getSpellIdByName(spellName):
	return spellName.replace(" ", "")

func _ready():
	pass
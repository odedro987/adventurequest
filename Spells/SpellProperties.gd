extends Node

var spellProperties = {
	"LaserBeam"  	:	{ 
								"Name"             : "Laser Beam",
								"BaseDamage"       : 30,
								"ManaCost"         : 40,
								"Attribute"        : global.ATTRIBUTES.FIRE,
								"Price"            : 300,
								"LevelRequirement" : 6,
								"ClassRequirement" : [ "Mage" ]
							},
	"IceShard"  	:	{ 
								"Name"             : "Ice Shard",
								"BaseDamage"       : 30,
								"ManaCost"         : 40,
								"Attribute"        : global.ATTRIBUTES.ICE,
								"Price"            : 300,
								"LevelRequirement" : 6,
								"ClassRequirement" : [ "Mage" ]
							},
	"LightningStrike"  	:	{ 
								"Name"             : "Lightning Strike",
								"BaseDamage"       : 30,
								"ManaCost"         : 40,
								"Attribute"        : global.ATTRIBUTES.LIGHTNING,
								"Price"            : 300,
								"LevelRequirement" : 6,
								"ClassRequirement" : [ "Mage" ]
							}
}
extends Node

var weaponProperties = {
	"BroadSword" 		:	{
								"Name"             : "Broad Sword",
								"BaseDamage"       : 10,
								"Attribute"        : global.ATTRIBUTES.GRASS,
								"Price"            : 90,
								"LevelRequirement" : 2,
								"ClassRequirement" : [ "Warrior" ]
							},
	"BattleHammer":	{
								"Name"             : "Battle Hammer",
								"BaseDamage"       : 20,
								"Attribute"        : global.ATTRIBUTES.FIRE,
								"Price"            : 240,
								"LevelRequirement" : 4,
								"ClassRequirement" : [ "Warrior" ]
							},
	"BattleAxe":	{
								"Name"             : "Battle Axe",
								"BaseDamage"       : 20,
								"Attribute"        : global.ATTRIBUTES.HOLY,
								"Price"            : 240,
								"LevelRequirement" : 4,
								"ClassRequirement" : [ "Warrior" ]
							},
	"Trident":	{
								"Name"             : "Trident",
								"BaseDamage"       : 30,
								"Attribute"        : global.ATTRIBUTES.WATER,
								"Price"            : 640,
								"LevelRequirement" : 8,
								"ClassRequirement" : [ "Warrior" ]
							},
	"Katana"  	:	{ 
								"Name"             : "Katana",
								"BaseDamage"       : 120,
								"Attribute"        : global.ATTRIBUTES.DARK,
								"Price"            : 3000,
								"LevelRequirement" : 20,
								"ClassRequirement" : [ "Ninja" ]
							},
	"WoodenStaff"  	:	{ 
								"Name"             : "Wooden Staff",
								"BaseDamage"       : 10,
								"Attribute"        : global.ATTRIBUTES.GRASS,
								"Price"            : 90,
								"LevelRequirement" : 2,
								"ClassRequirement" : [ "Mage" ]
							}
}
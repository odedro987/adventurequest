extends Node

#Variables.
var weaponProperties = WeaponProperties.weaponProperties
var weaponTypes = weaponProperties.keys()

#Returns the certain property of a certian weaponType from the hashmap.
func getProperty(weaponType, property):
	return weaponProperties[weaponType][property]

#Returns the weapon name without spaces.
func getWeaponIdByName(weaponName):
	return weaponName.replace(" ", "")

func _ready():
	pass
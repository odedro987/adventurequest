extends Node2D

#Gauges' variables.
export(Vector2) var hpVector = Vector2(100, 100)
export(Vector2) var mpVector = Vector2(100, 100)
export(Vector2) var expVector = Vector2(100, 100)
#Flipped bool.
export(bool) var flipped = false
#HUD name.
var name = ""
#Avatar size.
var spriteSize = 0
#HUD containers.
var nameContainer
var hpContainer
var mpContainer
var expContainer

func _ready():
	#Initializing avatar size.
	spriteSize = get_node("Sprite").get_texture().get_size()
	#Initializing containers.
	nameContainer = get_node("NameContainer")
	hpContainer = get_node("HpContainer")
	mpContainer = get_node("MpContainer")
	expContainer = get_node("ExpContainer")
	#Initializing HUD name.
	nameContainer.get_node("NameLabel").set_text(name)
	#Initializing Gauges' variables.
	setBar(hpContainer.get_node("HpLabel"), "HP", hpVector)
	setBar(mpContainer.get_node("MpLabel"), "MP", mpVector)
	setBar(expContainer.get_node("ExpLabel"), "EXP", expVector)
	#Flipping the HUD based on the flipped bool.
	if(flipped): 
		nameContainer.set_scale(Vector2(-1, 1))
		nameContainer.set_pos(Vector2(nameContainer.get_pos().x + nameContainer.get_size().x, nameContainer.get_pos().y))
		hpContainer.set_scale(Vector2(-1, 1))
		hpContainer.set_pos(Vector2(hpContainer.get_pos().x + hpContainer.get_size().x, hpContainer.get_pos().y))
		mpContainer.set_scale(Vector2(-1, 1))
		mpContainer.set_pos(Vector2(mpContainer.get_pos().x + mpContainer.get_size().x, mpContainer.get_pos().y))
	else:
		get_node("Extention").show()
		expContainer.show()

#Set function for barVectors.
func setBarVectorByType(barType, newVector):
	if(barType == "HP"): hpVector = newVector
	elif(barType == "MP"): mpVector = newVector
	else: expVector = newVector

#Get function for barVectors.
func getBarVectorByType(barType):
	if(barType == "HP"): return hpVector
	elif(barType == "MP"): return mpVector
	else: return expVector

#Sets the avatar.
func setAvatar(newAvatar):
	get_node("Avatar").set_texture(newAvatar)	

#Sets the HUD name.
func setName(newName):
	nameContainer.get_node("NameLabel").set_text(newName)

#Sets the bar by its type.	
func setBar(barTypeLabel, oldVectorType, newVector):
	if(newVector.x >= 0): setBarVectorByType(oldVectorType, newVector)
	else: setBarVectorByType(oldVectorType, Vector2(0, newVector.y))
	barTypeLabel.set_text(str(getBarVectorByType(oldVectorType).x) + "/" + str(getBarVectorByType(oldVectorType).y))
	update()

#Draws the gauges depending on their respective vars.
func _draw():
	draw_rect(Rect2(Vector2(34 - 12 - spriteSize.x / 2, 13 - 15 - spriteSize.y / 2), Vector2((hpVector.x / hpVector.y) * 30 * 4, 3 * 4)), Color(0.78, 0.117, 0.117, 1))
	draw_rect(Rect2(Vector2(34 - 12 - spriteSize.x / 2, 37 - 15 - spriteSize.y / 2), Vector2((mpVector.x / mpVector.y) * 30 * 4, 3 * 4)), Color(0.117, 0.243, 0.78, 1))
	if(!flipped): draw_rect(Rect2(Vector2(34 - 12 - spriteSize.x / 2, 61 - 15 - spriteSize.y / 2), Vector2((expVector.x / expVector.y) * 30 * 4, 3 * 4)), Color(0.011, 0.47, 0.063, 1))

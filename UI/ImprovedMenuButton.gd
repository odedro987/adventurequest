extends MenuButton

var selectedItemIndex
var items = {}

#Adds an item with multiple descriptions.
func addDescriptiveItem(name, descriptions):
	#Adding the new item to the end of the dictionary.
	items[items.size()] = [name, descriptions]
	var itemText = name
	var index = 0
	for description in range(descriptions.size()):
		if(index == 0): itemText += " (" + descriptions[index]
		elif(index == descriptions.size() - 1): itemText += "; " + descriptions[index] + ")"
		else: itemText += "; " + descriptions[index]
		index += 1
	get_popup().add_item(itemText)

#Clears all previous disabled items.
func clearItemDisability():
	for item in range(items.size()):
		get_popup().set_item_disabled(item, false)

#Updates selectedItemIndex
func updateSelectedItem(ID):
	selectedItemIndex = ID

#Returns selectedItem name.
func getSelectedItemName():
	return items[selectedItemIndex][0]

#Returns index by name.
func getItemIndexByName(name):
	for item in range(items.size()):
		if(items[item][0] == name): return item
	return -1

#Returns selectedItem description by its index.
func getSelectedItemDescription(index):
	return items[selectedItemIndex][1][index]

func _ready():
	pass
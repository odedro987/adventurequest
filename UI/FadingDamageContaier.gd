extends GridContainer

func showDamage(damage, attribute):
	restart()
	get_node("DamageText").set_text(damage)
	var damagePos = get_node("DamageText").get_pos()
	get_node("Attribute").set_texture(load("res://Assets/UI/Attributes/" + attribute + ".png"))
	get_node("Attribute").set_pos(Vector2(damagePos.x + 8 * damage.length(), damagePos.y - 36))
	show()
	get_node("AnimationPlayer").play("Fade")

func restart():
	get_node("AnimationPlayer").stop_all()
	set_opacity(1.0)
	hide()

func changePos(newPos):
	set_pos(newPos)

func _ready():
	get_node("DamageText").set_align(get_node("DamageText").ALIGN_CENTER)
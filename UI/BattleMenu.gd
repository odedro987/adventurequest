extends GridContainer

var weaponBank = preload("res://Weapons/WeaponBank.tscn").instance()
var spellBank = preload("res://Spells/SpellBank.tscn").instance()

func _ready():
	get_node("EquipmentMenu/ReturnButton").connect("pressed", self, "returnToBasicMenu")
	get_node("BasicMenu/EquipmentButton").connect("pressed", self, "goToEquipmentMenu")
	#Setting popup menus' font.
	get_node("BasicMenu/SpellsMenu").get_popup().add_font_override("font", load("res://Assets/UI/Fonts/PixelFont14.fnt"))
	get_node("EquipmentMenu/WeaponsMenu").get_popup().add_font_override("font", load("res://Assets/UI/Fonts/PixelFont14.fnt"))
	get_node("EquipmentMenu/ArmorsMenu").get_popup().add_font_override("font", load("res://Assets/UI/Fonts/PixelFont14.fnt"))
	get_node("EquipmentMenu/PetsMenu").get_popup().add_font_override("font", load("res://Assets/UI/Fonts/PixelFont14.fnt"))

func addSpells(spells):
	get_node("BasicMenu/SpellsMenu").get_popup().clear()
	for spell in spells:
		var spellID = spellBank.getSpellIdByName(spell)
		var attribute = spellBank.getProperty(spellID, "Attribute")
		var damage = spellBank.getProperty(spellID, "BaseDamage")
		var manaCost = spellBank.getProperty(spellID, "ManaCost")
		get_node("BasicMenu/SpellsMenu").addDescriptiveItem(spell, [str(damage) + " Att", attribute, str(manaCost) + " MP"])

func addWeapons(weapons):
	get_node("EquipmentMenu/WeaponsMenu").get_popup().clear()
	for weapon in weapons:
		var weaponID = weaponBank.getWeaponIdByName(weapon)
		var attribute = weaponBank.getProperty(weaponID, "Attribute")
		var damage = weaponBank.getProperty(weaponID, "BaseDamage")
		get_node("EquipmentMenu/WeaponsMenu").addDescriptiveItem(weapon, [str(damage) + " Att", attribute])

func returnToBasicMenu():
	get_node("BasicMenu").show()
	get_node("EquipmentMenu").hide()

func goToEquipmentMenu():
	get_node("BasicMenu").hide()
	get_node("EquipmentMenu").show()
extends AcceptDialog

func _ready():
	set_exclusive(true)
	var closeButtonPos = get_close_button().get_pos()
	get_close_button().set_pos(Vector2(closeButtonPos.x - 15, closeButtonPos.y + 25))
	get_node("RichTextLabel").ALIGN_CENTER

#Overrides and sets html tags supported text.
func set_text(newText):
	get_node("RichTextLabel").set_bbcode("[center]" + newText + "[/center]")

#Overrides and gets html tags supported text.
func get_text():
	get_node("RichTextLabel").get_bbcode()
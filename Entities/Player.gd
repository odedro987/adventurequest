extends Node2D

var weaponHoldingPos
var level
var maxHealth
var health
var maxMana
var mana
var name
var damage
var experience
var maxExperience

export var spells = []
export var weapons = []

func setPlayer(name, maxHealth, maxMana, experience, maxExperience, level, damage, spells, weapons):
	self.name = name
	self.maxHealth = maxHealth
	self.health = maxHealth
	self.maxMana = maxMana
	self.mana = maxMana
	self.experience = experience
	self.maxExperience = maxExperience
	self.level = level
	self.damage = damage
	self.spells = spells
	self.weapons = weapons
	
func _ready():
	weaponHoldingPos = Vector2(12 * 4, -((60 * 4) / 2) - 32 + 20)
	level = 1
	maxHealth = 80
	health = maxHealth
	maxMana = 80
	mana = maxMana
	name = "Oded"
	damage = 5
	experience = 50
	maxExperience = 100
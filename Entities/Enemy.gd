extends Node2D

var damage
var level
var maxHealth
var maxMana
var biome
var rarity
var attribute
var name

func _ready():
	damage = 0
	level = 0
	maxHealth = 0
	maxMana = 0
	biome = ""
	rarity = 0
	attribute = global.ATTRIBUTES.FIRE
	name = ""
extends Node

#Variables.
var monsterProperties = MonsterPropertiesDatabase.monsterProperties
var monsterTypes = monsterProperties.keys()
var minRarity = 1
var maxRarity = 4

#Returns a random monster by its rarity and or biome.
func getRandomMonsterByRarityAndOrBiome(rarity, biome):
	var monsters = getMonstersByRarityAndOrBiome(rarity, biome)
	randomize()
	return monsters[int(rand_range(0, monsters.size()))]

#Returns a list of monsters depending on rarity and or biome.
func getMonstersByRarityAndOrBiome(rarity, biome):
	var monsters = []
	if(biome != "all" && rarity == 0):
		for i in monsterTypes:
			if(getProperty(i, "Biome") == biome): monsters.push_front(i)
	elif(biome == "all" && rarity != 0):
		for i in monsterTypes:
			if(getProperty(i, "Rarity") == rarity): monsters.push_front(i)
	else:
		for i in monsterTypes:
			if(getProperty(i, "Rarity") == rarity && getProperty(i, "Biome") == biome): monsters.push_front(i)
	return monsters

#Returns the certain property of a certian monsterType from the hashmap.
func getProperty(monsterType, property):
	return monsterProperties[monsterType][property]
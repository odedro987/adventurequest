extends Node

var monsterProperties = {
	"BlueSlime" 		:	{
								"Name"          : "Slime",
								"Biome"         : "Grass",
								"BaseHealth"    : 5,
								"BaseDamage"    : 1,
								"Attribute"     : global.ATTRIBUTES.GRASS,
								"BaseFleeBlock" : 0,
								"Rarity"        : 1
							},
	"BigBlueSlime"  	:	{ 
								"Name"          : "Big Slime",
								"Biome"         : "Grass",
								"BaseHealth"    : 35,
								"BaseDamage"    : 7,
								"Attribute"     : global.ATTRIBUTES.GRASS,
								"BaseFleeBlock" : 2,
								"Rarity"        : 2
							},
	"HolySlime"  	:	{ 
								"Name"          : "Holy Slime",
								"Biome"         : "Grass",
								"BaseHealth"    : 50,
								"BaseDamage"    : 15,
								"Attribute"     : global.ATTRIBUTES.HOLY,
								"BaseFleeBlock" : 4,
								"Rarity"        : 3
							},
	"KingBlueSlime" 	:	{
								"Name"			: "King Slime",
								"Biome" 	 	: "Grass",
								"BaseHealth" 	: 160,
								"BaseDamage" 	: 15,
								"Attribute"  	: global.ATTRIBUTES.GRASS,
								"BaseFleeBlock" : 10,
								"Rarity" 		: 4
							},
	"Panther" 	:	{
								"Name"			: "Panther",
								"Biome" 	 	: "Grass",
								"BaseHealth" 	: 100,
								"BaseDamage" 	: 30,
								"Attribute"  	: global.ATTRIBUTES.DARK,
								"BaseFleeBlock" : 7,
								"Rarity" 		: 3
							},
	"Skeleton"  		:	{
								"Name"			: "Skeleton",
								"Biome" 		: "Dungeon",
								"BaseHealth" 	: 20,
								"BaseDamage" 	: 5,
								"Attribute" 	: global.ATTRIBUTES.DARK,
								"BaseFleeBlock" : 0,
								"Rarity" 		: 1
							},
	"SkeletonArrowed"	:	{
								"Name"			: "Undead Archer",
								"Biome" 		: "Dungeon",
								"BaseHealth" 	: 30,
								"BaseDamage" 	: 5,
								"Attribute" 	: global.ATTRIBUTES.DARK,
								"BaseFleeBlock" : 1,
								"Rarity" 		: 1
							},
	"BigBlackSlime" 	:	{
								"Name"			: "Black Slime",
								"Biome" 		: "Dungeon",
								"BaseHealth" 	: 40,
								"BaseDamage" 	: 3,
								"Attribute"		: global.ATTRIBUTES.DARK,
								"BaseFleeBlock"	: 2,
								"Rarity" 		: 2
							},
	"LavaSlime" 	:	{
								"Name"			: "Lava Slime",
								"Biome" 		: "Dungeon",
								"BaseHealth" 	: 70,
								"BaseDamage" 	: 15,
								"Attribute"		: global.ATTRIBUTES.FIRE,
								"BaseFleeBlock"	: 4,
								"Rarity" 		: 3
							},
	"IceSlime" 	:	{
								"Name"			: "Ice Slime",
								"Biome" 		: "Dungeon",
								"BaseHealth" 	: 70,
								"BaseDamage" 	: 15,
								"Attribute"		: global.ATTRIBUTES.ICE,
								"BaseFleeBlock"	: 4,
								"Rarity" 		: 3
							},
	"SkeletonPriest"	:	{
								"Name"			: "Undead Priest",
								"Biome"			: "Dungeon",
								"BaseHealth"	: 60,
								"BaseDamage"	: 10,
								"Attribute"		: global.ATTRIBUTES.DARK,
								"BaseFleeBlock"	: 3,
								"Rarity" 		: 3
							},
	"SkeletonHighPriest":	{
								"Name"			: "Papyrus",
								"Biome" 		: "Dungeon",
								"BaseHealth" 	: 100,
								"BaseDamage"	: 20,
								"Attribute" 	: global.ATTRIBUTES.DARK,
								"BaseFleeBlock" : 5,
								"Rarity" 		: 4
							}
}